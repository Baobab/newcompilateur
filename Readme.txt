Tout d'abord je me suis basé sur la correction de Mr. Jourlin pour pouvoir me concentrer sur la partie ForStatement 
	->Du coup j'ai rajouté la fonction 'forStatement' ainsi que la partie DOWNTO avec des restrictions : 
		-> Après le TO un INTEGER attendu
		-> Au départ je testais aussi les booleens sur cette partie mais il y avait des cas qu'il fallait traiter (cf la partie traitement booleen de mon programme)
			-> par exemple si on teste FOR a := 0 TO a == 2 DO ...
				-> Avec mon prog ce teste fonctionnera quand "a" sera égal a 2 on sortira de la boucle puisque expression retournera true et que l'on fait cmp $0, %rax c'est a dire si ce que retourne expression est faux.
			-> mais si on teste FOR a := 0 TO a < 2 DO ...
				-> Avec mon prog a < 2 est vrai puisqu'il vaut 0 donc le programme ne rentrera pas dans la boucle
				-> Il faudrait donc du coup pouvoir recuperer si le signe de l'expression et comparer avec TO ou DOWNTO, avec TO il ne faut pas le signe inférieur et avec DOWNTO le signe supérieur.
				
			->il est maitenant possible de le tester de cette manieres :
				FOR a := NUMBER TO/DOWNTO INTEGER DO ... 
				
	->Ajout du STEP avec des restrictions 
		-> Un NUMBER est attendu après le STEP (erreur traité sinon)
			-> FOR a := NUMBER TO/DOWNTO INTEGER STEP NUMBER DO ...



Voici des instances de test : 


#Test du DOWNTO avec STEP :
VAR a,b : INTEGER.

b:=10;
FOR a := 6 DOWNTO 0 STEP 2 DO
b := b-1.


#Test du TO avec STEP : 
VAR a,b : INTEGER.

b:=10;
FOR a := 0 TO 6 STEP 2 DO
b := b-1.


#Test du DOWNTO sans STEP : 
VAR a,b : INTEGER.

b:=10;
FOR a := 2 DOWNTO 0 DO
b := b-1.


#Test du TO sans STEP : 
VAR a,b : INTEGER.

b:=10;
FOR a := 0 TO 2 DO
b := b-1.





Voici des instances de test avec erreur : 

#Test du TO sans STEP : 
VAR a,b : INTEGER.

b:=10;
FOR a := 0 TO a == 2 DO
b := b-1.


#Test du TO avec  STEP : 
VAR a,b : INTEGER.

b:=10;
FOR a := 0 TO 6 STEP 2.1 DO
b := b-1.
